package nl.rickmartens.easybungeecordmsg.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import nl.rickmartens.easybungeecordmsg.EasyBungeecordMSG;

public class MSGCommand extends Command {

    public MSGCommand() {
        super("msg");
    }

    @SuppressWarnings({ "deprecation", "unlikely-arg-type" })
    public void execute(CommandSender sender, String[] args) {
        if ((sender instanceof ProxiedPlayer)) {
            ProxiedPlayer p = (ProxiedPlayer)sender;
            if (args.length == 0 || args.length == 1) {
                p.sendMessage(EasyBungeecordMSG.prefixMSG + "§cUsage§8: §e/msg <Player> <Message> §7to send a private message to a player.");
                return;
            }

            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
            if (target != null) {
                if (p != target) {
                    if (!ToggleMSGCommand.msgtoggle.contains(target.getName())) {
                        String msg = "";
                        for (int i = 1; i < args.length; i++) {
                            msg = msg + args[i] + " ";
                        }
                        ReplyCommand.reply.put(p, target);
                        ReplyCommand.reply.put(target, p);
                        p.sendMessage(EasyBungeecordMSG.prefixMSG + "§7To §c" + target.getName() + " §8» §7" + msg);
                        target.sendMessage(EasyBungeecordMSG.prefixMSG + "§7From §c" + p.getName() + " §8» §7" + msg);
                    } else if (p.hasPermission("easybungeemsg.togglemsg.bypass")) {
                        String msg = "";
                        for (int i = 1; i < args.length; i++) {
                            msg = msg + args[i] + " ";
                        }
                        ReplyCommand.reply.put(p, target);
                        ReplyCommand.reply.put(target, p);
                        p.sendMessage(EasyBungeecordMSG.prefixMSG + "§7To §c" + target.getName() + " §8» §7" + msg);
                        target.sendMessage(EasyBungeecordMSG.prefixMSG + "§7From §c" + p.getName() + " §8» §7" + msg);
                    } else {
                        p.sendMessage(EasyBungeecordMSG.prefixMSG + "§7That person has his private messages turned §4§lOFF §7.");
                    }
                } else {
                    p.sendMessage(EasyBungeecordMSG.prefixMSG + "§7You can't send private messages to yourself.");
                }
            } else {
                p.sendMessage(EasyBungeecordMSG.prefixMSG + "§7That player isn't online!");
            }
        }
    }

}