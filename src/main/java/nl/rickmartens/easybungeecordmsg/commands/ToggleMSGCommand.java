package nl.rickmartens.easybungeecordmsg.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import nl.rickmartens.easybungeecordmsg.EasyBungeecordMSG;

import java.util.ArrayList;

public class ToggleMSGCommand extends Command {

    public static ArrayList<String> msgtoggle = new ArrayList<String>();

    public ToggleMSGCommand() {
        super("togglemsg");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)){
            sender.sendMessage(EasyBungeecordMSG.prefixMSG + "§4§lYou need to be a player to execute this command!");
            return;
        }

        ProxiedPlayer p = (ProxiedPlayer) sender;

        if (!p.hasPermission("easybungeemsg.togglemsg")) {
            p.sendMessage(EasyBungeecordMSG.noPermMSG);
            return;
        }

        if (!msgtoggle.contains(p.getName())) {
            msgtoggle.add(p.getName());
            p.sendMessage(EasyBungeecordMSG.prefixMSG + "You have now your Private Messages turned §4§lOFF§7!");
        } else {
            msgtoggle.remove(p.getName());
            p.sendMessage(EasyBungeecordMSG.prefixMSG + "You have now your Private Messages turned §a§lON§7!");
        }
    }

}

