package nl.rickmartens.easybungeecordmsg;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Plugin;
import nl.rickmartens.easybungeecordmsg.commands.MSGCommand;
import nl.rickmartens.easybungeecordmsg.commands.ReplyCommand;
import nl.rickmartens.easybungeecordmsg.commands.ToggleMSGCommand;

public final class EasyBungeecordMSG extends Plugin {

    public static EasyBungeecordMSG pl;

    public static String noPermMSG = ChatColor.translateAlternateColorCodes('&', "&6&lMSG &8&l• &cYou don't have the permission for doing this!");
    public static String prefixMSG = ChatColor.translateAlternateColorCodes('&', "&6&lMSG &8&l• &7");

    @Override
    public void onEnable() {
        pl = this;

        getProxy().getPluginManager().registerCommand(this, new ToggleMSGCommand());
        getProxy().getPluginManager().registerCommand(this, new MSGCommand());
        getProxy().getPluginManager().registerCommand(this, new ReplyCommand());

        getProxy().getConsole().sendMessage("[EasyBCMSG] Successful turned " + ChatColor.DARK_GREEN + "ON " + ChatColor.RESET + "EasyBungeeCordMSG v" + getDescription().getVersion());
    }

    @Override
    public void onDisable() {
        getProxy().getConsole().sendMessage("[EasyBCMSG] Successful turned " + ChatColor.DARK_RED + "OFF " + ChatColor.RESET + "EasyBungeeCordMSG v" + getDescription().getVersion());
    }
}
